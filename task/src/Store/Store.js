import {EventEmitter} from "events";
import dispatcher from "../dispatcher";

class Store extends EventEmitter{
    state = {
        tvitovi: [],
        user:[]
    };
    getUser(){
        return this.state.user;
    }
    getAll(){
        return this.state.tvitovi;
    }
    crateTweet(naziv,userID,handle,image){
        let date=new Date().toLocaleString();
        this.state.tvitovi.reverse().push({
            id: JSON.stringify(date),
            userID,
            handle,
            naziv,
            image,
            postdate: JSON.stringify(date)
          });
        this.emit("change");
    }

    handleActions(action){
        switch(action.type){
            case "NEW_TWEET":{
                this.crateTweet(action.naziv,action.userID,action.handle,action.image);
                break;
            }
            case "GET_ALL_TWEETS":{
                this.state.tvitovi = action.data.data.reverse();                
                this.emit("change");
                break;
            }
            case "GET_USER":{
                this.state.user = action.data.data;
                this.emit("change");
                break;
            }
            default:{
                break;
            }
        }
    }

}
const store=new Store();
dispatcher.register(store.handleActions.bind(store));
export default store;