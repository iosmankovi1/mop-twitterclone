import React, { Component } from 'react';
import Side from './components/ProfileBox/Side';
import NewTweet from './components/TweetInput/NewTweet';
import AllTweets from './components/TweetList/AllTweets';
import Futer from './components/Futer/Futer';
import './App.css';
import Store from "./Store/Store";
import * as Actions from "./actions/Actions";

class App extends Component {
    state = {
      tvitovi: [] ,
      user:[]
    };

//geting tweets and user with id=5c94d4ecebb662339876adcd from database 
  componentDidMount() {    
    Store.on("change",()=>{
      this.setState({
        tvitovi: Store.getAll()
      });
    });  
    Store.on("change",()=>{
      this.setState({
        user: Store.getUser()
      });
    });  
    Actions.getTweets("5c94d4ecebb662339876adcd"); 
    Actions.getUserById("5c94d4ecebb662339876adcd");   
  }
  
//Adding new tweet
newTweet=(naziv)=>{  
  let date=new Date().toLocaleString();
  const novi={
    id: JSON.stringify(date),
    userID: "5c94d4ecebb662339876adcd",
    handle: "@irma",
    naziv,
    image: "./logo.jpg",
    postdate: JSON.stringify(date)
  };
  this.setState({ tvitovi: [...this.state.tvitovi,novi] });
  Actions.crateTweet(naziv,"5c94d4ecebb662339876adcd","@irma","./logo.jpg");
}

  render() {
    return (
      <div className="App">
        <div className="side">
          <Side tvitovi={this.state.tvitovi} user={this.state.user} />
        </div>
        <div className="main">
          <div className="nt">
            <NewTweet  newTweet={this.newTweet} />
          </div>
          <div className="at">
            <AllTweets tvitovi={this.state.tvitovi}/>
          </div>          
        </div>
        <div className="futer">
          <Futer/>
        </div>
        
      </div>
    );
  }
}

export default App;
