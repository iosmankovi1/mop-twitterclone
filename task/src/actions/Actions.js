import dispatcher from "../dispatcher";
import axios from "axios";

export function crateTweet(naziv,userID,handle,image) {
    axios.post("http://localhost:8000/putData", {
    userID:userID,
    handle:handle,
    naziv:naziv,
    image:image
    }); 
    dispatcher.dispatch({
        type: "NEW_TWEET",
        naziv,
        userID,
        handle,
        image
    })
}

export function getTweets(id){   
    axios.get("http://localhost:8000/getData/"+id)
        .then(data => {;        
            dispatcher.dispatch({type: "GET_ALL_TWEETS",data});   
    });    
}

export function getUserById(user){ 
    axios.get("http://localhost:8000/getUser/"+user)
        .then(data => {       
            dispatcher.dispatch({type: "GET_USER",data});    
    });    
}