import React, { Component } from 'react'
import './Input.css'

export class NewTweet extends Component {
  state={
    naziv: ''
  }
  onChange = (e)=>this.setState({ naziv: e.target.value });
  onSubmit = (e) => {
    e.preventDefault();
    this.props.newTweet(this.state.naziv);
    this.setState( { naziv: ''} );
  }
  render() {
    return (
        <form onSubmit={this.onSubmit}>
        <input 
          type="text"
          name="Unos"
          placeholder=" Please enter your tweet and press Enter"
          value={this.state.naziv}
          onChange={this.onChange}
        />
      </form>
    )
  }
}

export default NewTweet
