import React, { Component } from 'react'
import PropTypes from 'prop-types';
import './Tweet.css';
import Moment from 'react-moment';

export class Tweet extends Component {
  render() {  
    return (
      <div className="tvit">
        <img ref="image" src={require("./logo.jpg")} alt="Logo"  />
        <p>
          {this.props.tvit.handle} {}
          <Moment fromNow>
            {this.props.tvit.postdate}
          </Moment>
        </p>
        
        <p>
          {this.props.tvit.naziv}
        </p>
        
      </div>
      
    )
  }
}

Tweet.propTypes={
  tvit: PropTypes.object.isRequired
}
export default Tweet
