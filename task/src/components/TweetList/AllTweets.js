import React, { Component } from 'react';
import Tweet from './Tweet';
import PropTypes from 'prop-types';


export class AllTweets extends Component {
  render() {
    return this.props.tvitovi.reverse().map((x)=>(
      <Tweet key={x.id} tvit={x} />
    ));
  }
}

AllTweets.propTypes={
  tvitovi: PropTypes.array.isRequired
}

export default AllTweets
