import React, { Component } from 'react'
import './Side.css'
import PropTypes from 'prop-types';
export class Side extends Component {
  render() {

    return (
      <div className="side">
        <img src={require("./logo.jpg")} alt="Logo"  />
        <div className="description">
          {this.props.user.map(x=>(<p>{x.username}</p>))}
          {this.props.user.map(x=>(<p>{x.handle}</p>))}
          <p className="tvitovi">
            Tweets { this.props.tvitovi.length}
          </p>
        </div>        
      </div>
    )
  }
}
Side.propTypes={
  tvitovi: PropTypes.array.isRequired,
  user: PropTypes.array.isRequired
}
export default Side
