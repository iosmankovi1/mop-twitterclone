const express = require("express");
const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('components'));

var cors = require('cors');
app.use(cors()) ;
//connect to database
const mongo = require("mongoose");
const Data = require("./tweetsDB");
const User=require("./users");
mongo.connect(
  'mongodb+srv://irma:IrmaO@cluster0-qfinn.mongodb.net/test?retryWrites=true',
  { useNewUrlParser: true }
);
//geting data from db
app.get('/getData/:userID', (req, res) => {
  Data.find({userID: req.params.userID})
  .exec()
  .then((rez)=>{
    res.status(200).json(rez);
  })
  .catch((err)=>{
    console.log(err);
    res.status(500).json({error: err});
  });
});
//get user
app.get('/getUser/:userID',(req, res) => {
  User.find({_id: req.params.userID })
    .then((rez)=>{
      res.status(200).json(rez);
    })
});
//adding new tweet in database 
app.post("/putData", (req, res) => {
  const tweet=new Data({
    _id:new mongo.Types.ObjectId(),
    userID: req.body.userID,
    handle: req.body.handle,
    naziv: req.body.naziv,
    image: req.body.image ,
  });

  tweet.save().then(resoult=>{
    console.log(resoult);
  })
  .catch(err=> console.log(err));

});

app.listen(8000);
