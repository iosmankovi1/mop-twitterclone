const mongo = require("mongoose");
const Schema = mongo.Schema;

const TweetsSchema = new Schema(
  {
    _id: mongo.Schema.Types.ObjectId,
    userID: String,
    handle: String,
    naziv: String,
    image: String,
    postdate:{ type: Date, default: Date.now }
  },
  { timestamps: false }
);

module.exports = mongo.model("Data", TweetsSchema);
