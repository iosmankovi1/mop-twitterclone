const mongo = require("mongoose");
const Schema = mongo.Schema;

const UserSchema = new Schema(
  {
    _id: mongo.Schema.Types.ObjectId,
    username:String,
    handle: String,
    image: String
  },
  { timestamps: true }
);

module.exports = mongo.model("User", UserSchema);
